CREATE DATABASE conferenciaJWT;
USE conferenciaJWT;
create table users(
	id INT(11) not null auto_increment primary key,
	email VARCHAR(50) UNIQUE NOT NULL,
    password VARCHAR(100) NOT NULL,
    rol ENUM('admin', 'user') not null default 'user',
    status enum('pending', 'approved', 'rejected') not null default 'pending'
);
create table contacts(
	id int(11) not null auto_increment primary key,
    name varchar(50) unique not null,
    phone numeric(10) not null,
    user_id int(11),
    foreign key (user_id) references users(id)
);