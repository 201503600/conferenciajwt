import React from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import Dialogo from './Dialogo';
import Divider from '@material-ui/core/Divider';

import '../Styles/Registro.css'
import ruta from './Ruta';
import jwt_decode from 'jwt-decode';

class Registro extends React.Component{

    state = {
      mensajeError: '',
      error: false,
      registroDatos: {
        userName: '',
        password: '',
        copyPassword: '',
        emailError: false,
      },
      open: false,
      openConfirmacion: false
    }

    componentDidMount() {
      const token = localStorage.getItem('token');
      if (token !== null)
        this.verifyRol(token);
    }

    verifyRol = (token) => {
      const userRole = jwt_decode(token);

      const expiracion = userRole.exp;
      if (expiracion < Date.now() / 1000){
        // El token ha expirado se cierra la sesión
        localStorage.clear();
        return;
      }

      console.log(userRole)
      if (userRole === 'admin')
        window.location.href = `/admin`;
      else
        window.location.href = `/inicio`;
    }

    goInicio = async() => {
      if (this.state.registroDatos.emailError){
        this.abrirConfirmacion('ERROR','Ingrese un correo electrónico válido');
        return;
      }

      try {
        let config = {
          method: 'POST',
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': '*',
              'Access-Control-Allow-Methods': 'POST, GET',
              'Access-Control-Request-Method': '*',
              'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
          },
          body: JSON.stringify({
              email: this.state.registroDatos.userName,
              pass: this.state.registroDatos.password,
          }),
          mode: 'cors'
        }
  
        let response = await fetch('http://' + ruta.ip + ':' + ruta.port + '/api/register', config)
        let data = await response.json();
        console.log(data);
        if (response.status == 200){
          this.abrirConfirmacion('REGISTRO','Se registro exitosamente y se redirige a login');
          setTimeout(()=>{
            this.goLogin();
        }, 3000);
        }else if (response.status == 400){
          this.abrirConfirmacion('ERROR',`Ya existe un usuario con username ${this.state.registroDatos.userName}`);
        }else{
          this.abrirConfirmacion('ERROR','Ocurrio un error! Intente de nuevo');
        }  
      } catch (error) {
        this.abrirConfirmacion('ERROR','Ocurrio un error! Intente de nuevo');
      }
    }
    
    setUserName = async e  => {
      this.state.registroDatos.userName = e.target.value;

      const correoElectronicoRegExp = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
      this.state.registroDatos.emailError = !correoElectronicoRegExp.test(e.target.value);
    }

    setPass = async e  => {
      this.state.registroDatos.password = e.target.value;
      this.compararPass();
    }

    setCopyPass = async e  => {
      this.state.registroDatos.copyPassword = e.target.value;
      this.compararPass();
    }

    compararPass = () => {
      if(this.state.registroDatos.password !== this.state.registroDatos.copyPassword){
        this.setState({
          mensajeError: 'La contraseña no coincide',
          error: true
        });
      }else{
        this.setState({
          mensajeError: '',
          error: false
        });
      }
    }

    abrirDialogo = () => {
      this.setState({
        open: true
      })
    }

    cerrarDialogo = () => {
      this.setState({
        open: false
      })
    }

    abrirConfirmacion = (titulo, mensaje) => {
      this.titulo = titulo;
      this.mensaje = mensaje;
      this.setState({
        openConfirmacion: true
      })
    }

    cerrarConfirmacion = () => {
      this.setState({
        openConfirmacion: false
      })
    }

    goLogin(){
      window.location.href = `/`;
    }

    render() {
        return (
          <Grid container className="root" justifyContent="center">
            <Paper className="paper" elevation={5} justify="center">
              <h1>REGISTRO</h1>
              <Divider/>
              <Grid container style={{paddingBottom: "0em"}}>
                <Grid item xs={12} sm={7} style={{paddingLeft: "3em", paddingRight: "3em", paddingBottom: "0em"}}>
                  <TextField
                    label="Correo electrónico" fullWidth
                    margin="normal" onChange={this.setUserName}
                  />
                  <TextField
                    label="Password" type="password"
                    fullWidth margin="normal" onChange={this.setPass}
                  />
                  <TextField
                    label="Confirmar password" type="password"
                    helperText={this.state.mensajeError} error={this.state.error}
                    fullWidth margin="normal" onChange={this.setCopyPass}
                  />
                </Grid>
              </Grid>
              <Grid container justifyContent="center" >
                <Button variant="outlined" color="primary" onClick={this.goInicio}>
                  Registrarse
                </Button>
              </Grid>
              <Grid container className="root" justifyContent="center" style={{paddingBottom: "1em"}}>
                <Typography >
                  <Link href="#" onClick={this.goLogin}>
                    ¿Ya tienes cuenta? Logeate
                  </Link>
                </Typography>
              </Grid>
            </Paper>
            <Dialogo 
              titulo = {this.titulo}
              mensaje = {this.mensaje}
              open = {this.state.openConfirmacion}
              cerrar = {this.cerrarConfirmacion}
            />
          </Grid>
        );
    }
}

export default Registro;