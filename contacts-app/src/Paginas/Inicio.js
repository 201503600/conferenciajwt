import React from "react";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import Dialogo from "./Dialogo";

import "../Styles/Admin.css";
import ruta from "./Ruta";
import jwt_decode from "jwt-decode";
import {
  Box,
  Divider,
  Table,
  TableHead,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
} from "@material-ui/core";

export default class Inicio extends React.Component {
  intervalId;
  state = {
    contacts: [],
    openDialogo: false,
  };

  async componentDidMount() {
    const token = localStorage.getItem("token");
    if (token !== null) this.verifyRol(token);
    else {
      window.location.href = `/login`;
      return;
    }
    await this.fetchConsulta();
    this.intervalId = setInterval(async () => {
      await this.fetchConsulta();
    }, 5000);
  }

  componentWillUnmount() {
    clearInterval(this.intervalId);
  }

  verifyRol = (token) => {
    const userRole = jwt_decode(token);

    const expiracion = userRole.exp;
    if (expiracion < Date.now() / 1000) {
      // El token ha expirado se cierra la sesión
      localStorage.clear();
      this.abrirDialogo("CIERRE DE SESIÓN", "Su sesión ha expirado");
      setTimeout(() => {
        window.location.href = `/login`;
      }, 3000);
    }

    if (userRole.role !== "user") window.location.href = `/access-denied`;
  };

  fetchConsulta = async () => {
    try {
      const options = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
          "Content-Type": "application/json",
        },
      };
      let req = await fetch(
        "http://" + ruta.ip + ":" + ruta.port + "/api/users",
        options
      );
      let data = await req.json();
      console.log(data);
      if (req.status == 200) {
        this.setState({
          contacts: data,
        });
      } else if (req.status == 400) {
        this.abrirDialogo("ERROR", "Credenciales incorrectas!");
      } else {
        this.abrirDialogo("ERROR", "Ocurrio un error! Intente de nuevo");
      }
    } catch (error) {
      this.abrirDialogo("ERROR", "Ocurrio un error! Intente de nuevo");
    }
  };

  handleEliminar = async (contactId) => {
    let config = {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "*",
        "Access-Control-Request-Method": "*",
        "Access-Control-Allow-Headers":
          "Origin, X-Requested-With, Content-Type, Accept, Authorization",
      },
      mode: "cors",
    };
    //console.log(config.body.photo);
    try {
      let response = await fetch(
        `http://${ruta.ip}:${ruta.port}/api/users/${contactId}`,
        config
      );
      await response.json();
      if (response.status == 200) {
        this.abrirDialogo(
          "CONTACTO ELIMINADO",
          "Se eliminó el contacto exitosamente!"
        );
      } else {
        this.abrirDialogo("ERROR", "Ocurrio un error! Intente de nuevo");
      }
      await this.fetchConsulta();
    } catch (error) {
      this.abrirDialogo("ERROR", "Ocurrio un error! Intente de nuevo");
    }
  };

  goCreateContact() {
    window.location.href = `/contact`;
  }

  abrirDialogo = (titulo, mensaje) => {
    this.titulo = titulo;
    this.mensaje = mensaje;
    this.setState({
      openDialogo: true,
    });
  };

  cerrarDialogo = () => {
    this.setState({
      openDialogo: false,
    });
  };

  closeSession() {
    localStorage.clear();
    window.location.href = `/login`
  }

  render() {
    return (
      <div className="root">
        <Paper className="paper" elevation={5}>
          <Container
            className="container"
            maxWidth="md"
            style={{
              display: "block",
              alignItems: "center",
              justifyContent: "center",
              height: "100vh",
            }}
          >
            <Box textAlign="center">
              <Typography variant="h4" component="h1" gutterBottom>
                Lista de contactos
              </Typography>
            </Box>
            <Box textAlign="center">
              <Button
                variant="contained"
                color="primary"
                className="button"
                onClick={() => this.goCreateContact()}
              >
                Crear nuevo contacto
              </Button>
              <Button
                variant="contained"
                color="default"
                className="button"
                onClick={() => this.closeSession()}
              >
                Cerrar sesión
              </Button>
            </Box>
            <br></br>
            <Divider />

            <TableContainer component={Paper}>
              <Table style={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>No.</TableCell>
                    <TableCell align="center">Nombre</TableCell>
                    <TableCell align="center">Teléfono</TableCell>
                    <TableCell align="center"></TableCell>
                    <TableCell align="center"></TableCell>
                  </TableRow>
                </TableHead>

                <TableBody>
                  {this.state.contacts.map((contact, index) => (
                    <TableRow className="userCard" key={contact.id}>
                      <TableCell component="th" scope="row">
                        {index + 1}
                      </TableCell>
                      <TableCell align="center">{contact.name}</TableCell>
                      <TableCell align="center">{contact.phone}</TableCell>

                      <TableCell>
                        <Button
                          variant="contained"
                          color="secondary"
                          className="button"
                          onClick={() => this.handleEliminar(contact.id)}
                        >
                          Eliminar
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Container>
        </Paper>
        <Dialogo
          titulo={this.titulo}
          mensaje={this.mensaje}
          open={this.state.openDialogo}
          cerrar={this.cerrarDialogo}
        />
      </div>
    );
  }
}
