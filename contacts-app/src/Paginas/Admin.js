import React from "react";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import Dialogo from "./Dialogo";

import "../Styles/Admin.css";
import ruta from "./Ruta";
import jwt_decode from "jwt-decode";
import {
  Box,
  Divider,
  Table,
  TableHead,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
} from "@material-ui/core";

export default class Admin extends React.Component {
  intervalId;
  state = {
    users: [],
    openDialogo: false,
  };

  async componentDidMount() {
    const token = localStorage.getItem("token");
    if (token !== null) this.verifyRol(token);
    else {
      window.location.href = `/login`;
      return;
    }
    await this.fetchConsulta();
    this.intervalId = setInterval(async () => {
      await this.fetchConsulta();
    }, 5000);
  }

  componentWillUnmount() {
    clearInterval(this.intervalId);
  }

  verifyRol = (token) => {
    const userRole = jwt_decode(token);

    const expiracion = userRole.exp;
    if (expiracion < Date.now() / 1000) {
      // El token ha expirado se cierra la sesión
      localStorage.clear();
      this.abrirDialogo("CIERRE DE SESIÓN", "Su sesión ha expirado");
      setTimeout(() => {
        window.location.href = `/login`;
      }, 3000);
    }

    if (userRole.role !== "admin") window.location.href = `/access-denied`;
  };

  fetchConsulta = async () => {
    try {
      const options = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
          "Content-Type": "application/json",
        },
      };
      let req = await fetch(
        "http://" + ruta.ip + ":" + ruta.port + "/api/admin",
        options
      );
      let data = await req.json();
      //console.log(data);
      if (req.status == 200) {
        this.setState({
          users: data,
        });
      } else if (req.status == 400) {
        this.abrirDialogo("ERROR", "Credenciales incorrectas!");
      } else {
        this.abrirDialogo("ERROR", "Ocurrio un error! Intente de nuevo");
      }
    } catch (error) {
      this.abrirDialogo("ERROR", "Ocurrio un error! Intente de nuevo");
    }
  };

  handleAprobar = async (userId) => {
    let config = {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "*",
        "Access-Control-Request-Method": "*",
        "Access-Control-Allow-Headers":
          "Origin, X-Requested-With, Content-Type, Accept, Authorization",
      },
      body: JSON.stringify({
        status: "approved",
      }),
      mode: "cors",
    };
    //console.log(config.body.photo);
    try {
      let response = await fetch(
        `http://${ruta.ip}:${ruta.port}/api/admin/${userId}`,
        config
      );
      await response.json();
      if (response.status == 200) {
        this.abrirDialogo(
          "SOLICITUD ACEPTADA",
          "Se aprobó la solicitud del usuario!"
        );
      } else {
        this.abrirDialogo("ERROR", "Ocurrio un error! Intente de nuevo");
      }
      await this.fetchConsulta();
    } catch (error) {
      this.abrirDialogo("ERROR", "Ocurrio un error! Intente de nuevo");
    }
  };

  handleRechazar = async (userId) => {
    let config = {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "POST, GET",
        "Access-Control-Request-Method": "*",
        "Access-Control-Allow-Headers":
          "Origin, X-Requested-With, Content-Type, Accept, Authorization",
      },
      body: JSON.stringify({
        status: "rejected",
      }),
      mode: "cors",
    };
    //console.log(config.body.photo);
    try {
      let response = await fetch(
        `http://${ruta.ip}:${ruta.port}/api/admin/${userId}`,
        config
      );
      await response.json();
      if (response.status == 200) {
        this.abrirDialogo(
          "SOLICITUD DENEGADA",
          "Se rechazó la solicitud del usuario!"
        );
      } else {
        this.abrirDialogo("ERROR", "Ocurrio un error! Intente de nuevo");
      }
      await this.fetchConsulta();
    } catch (error) {
      this.abrirDialogo("ERROR", "Ocurrio un error! Intente de nuevo");
    }
  };

  abrirDialogo = (titulo, mensaje) => {
    this.titulo = titulo;
    this.mensaje = mensaje;
    this.setState({
      openDialogo: true,
    });
  };

  cerrarDialogo = () => {
    this.setState({
      openDialogo: false,
    });
  };

  closeSession() {
    localStorage.clear();
    window.location.href = `/login`
  }

  render() {
    return (
      <div className="root">
        <Paper className="paper" elevation={5}>
          <Container
            className="container"
            maxWidth="md"
            style={{
              display: "block",
              alignItems: "center",
              justifyContent: "center",
              height: "100vh",
            }}
          >
            <Box textAlign="center">
              <Typography variant="h4" component="h1" gutterBottom>
                Lista de usuarios
              </Typography>
            </Box>
            <Box textAlign="center">
              <Button
                variant="contained"
                color="default"
                className="button"
                onClick={() => this.closeSession()}
              >
                Cerrar sesión
              </Button>
            </Box>
            <Divider />

            <TableContainer component={Paper}>
              <Table style={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>No.</TableCell>
                    <TableCell align="center">Email</TableCell>
                    <TableCell align="center">Estado</TableCell>
                    <TableCell align="center"></TableCell>
                    <TableCell align="center"></TableCell>
                  </TableRow>
                </TableHead>

                <TableBody>
                  {this.state.users.map((user, index) => (
                    <TableRow className="userCard" key={user.id}>
                      <TableCell component="th" scope="row">
                        {index + 1}
                      </TableCell>
                      <TableCell align="center">{user.email}</TableCell>
                      <TableCell align="center">{user.status}</TableCell>

                      {user.status === "pending" ? (
                        <>
                          <TableCell>
                            <Button
                              variant="contained"
                              color="primary"
                              className="button"
                              onClick={() => this.handleAprobar(user.id)}
                            >
                              Aprobar
                            </Button>
                          </TableCell>
                          <TableCell>
                            <Button
                              variant="contained"
                              color="secondary"
                              className="button"
                              onClick={() => this.handleRechazar(user.id)}
                            >
                              Rechazar
                            </Button>
                          </TableCell>
                        </>
                      ) : (
                        <>
                          <TableCell></TableCell>
                          <TableCell></TableCell>
                        </>
                      )}
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Container>
        </Paper>
        <Dialogo
          titulo={this.titulo}
          mensaje={this.mensaje}
          open={this.state.openDialogo}
          cerrar={this.cerrarDialogo}
        />
      </div>
    );
  }
}
