import React from "react";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Dialogo from "./Dialogo";
import Divider from "@material-ui/core/Divider";

import "../Styles/Registro.css";
import ruta from "./Ruta";
import jwt_decode from "jwt-decode";

class Contacto extends React.Component {
  state = {
    mensajeError: "",
    error: false,
    registroDatos: {
      name: "",
      phone: "",
      phoneError: false,
    },
    open: false,
    openConfirmacion: false,
  };

  componentDidMount() {
    const token = localStorage.getItem("token");
    if (token !== null) this.verifyRol(token);
    else {
      window.location.href = `/login`;
      return;
    }
  }

  verifyRol = (token) => {
    const userRole = jwt_decode(token);

    const expiracion = userRole.exp;
    if (expiracion < Date.now() / 1000) {
      // El token ha expirado se cierra la sesión
      localStorage.clear();
      this.abrirDialogo("CIERRE DE SESIÓN", "Su sesión ha expirado");
      setTimeout(() => {
        window.location.href = `/login`;
      }, 3000);
    }
    if (userRole.role !== "user") window.location.href = `/access-denied`;
  };

  createContact = async () => {
    if (this.state.registroDatos.phoneError) {
      this.abrirConfirmacion("ERROR", "Ingrese un teléfono válido");
      return;
    }
    if (this.state.registroDatos.name.trim() === "") {
      this.abrirConfirmacion("ERROR", "Llene todos los campos");
      return;
    }

    try {
      let config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "POST, GET",
          "Access-Control-Request-Method": "*",
          "Access-Control-Allow-Headers":
            "Origin, X-Requested-With, Content-Type, Accept, Authorization",
        },
        body: JSON.stringify({
          name: this.state.registroDatos.name,
          phone: this.state.registroDatos.phone,
        }),
        mode: "cors",
      };

      let response = await fetch(
        "http://" + ruta.ip + ":" + ruta.port + "/api/users",
        config
      );
      if (response.status == 200) {
        this.abrirConfirmacion(
          "CONTACTO CREADO",
          "Se creó el contacto exitosamente"
        );
        setTimeout(() => {
          this.goLogin();
        }, 3000);
      } else if (response.status == 400) {
        this.abrirConfirmacion("ERROR", `Ocurrio un error! Intente de nuevo`);
      } else {
        this.abrirConfirmacion("ERROR", "Ocurrio un error! Intente de nuevo");
      }
    } catch (error) {
        console.log(error)
      this.abrirConfirmacion("ERROR", "Ocurrio un error! Intente de nuevo");
    }
  };

  setUserName = async (e) => {
    this.state.registroDatos.name = e.target.value;
  };

  setPhone = async (e) => {
    this.state.registroDatos.phone = e.target.value;

    const phoneRegExp = /^\d{8,10}$/;
    this.state.registroDatos.phoneError = !phoneRegExp.test(e.target.value);
  };

  abrirDialogo = () => {
    this.setState({
      open: true,
    });
  };

  cerrarDialogo = () => {
    this.setState({
      open: false,
      registroDatos: {
        phoneError: false
      }
    });
  };

  abrirConfirmacion = (titulo, mensaje) => {
    this.titulo = titulo;
    this.mensaje = mensaje;
    this.setState({
      openConfirmacion: true,
    });
  };

  cerrarConfirmacion = () => {
    this.setState({
      openConfirmacion: false,
    });
  };

  goLogin() {
    window.location.href = `/inicio`;
  }

  render() {
    return (
      <Grid container className="root" justifyContent="center">
        <Paper className="paper" elevation={5} justify="center">
          <h1>NUEVO CONTACTO</h1>
          <Divider />
          <Grid container style={{ paddingBottom: "0em" }}>
            <Grid
              item
              xs={12}
              sm={7}
              style={{
                paddingLeft: "3em",
                paddingRight: "3em",
                paddingBottom: "0em",
              }}
            >
              <TextField
                label="Nombre"
                fullWidth
                margin="normal"
                onChange={this.setUserName}
              />
              <TextField
                label="Teléfono"
                type="number"
                fullWidth
                margin="normal"
                onChange={this.setPhone}
              />
            </Grid>
          </Grid>
          <Grid container justifyContent="center">
            <Button
              variant="outlined"
              color="primary"
              onClick={this.createContact}
            >
              Crear contacto
            </Button>
          </Grid>
        </Paper>
        <Dialogo
          titulo={this.titulo}
          mensaje={this.mensaje}
          open={this.state.openConfirmacion}
          cerrar={this.cerrarConfirmacion}
        />
      </Grid>
    );
  }
}

export default Contacto;
