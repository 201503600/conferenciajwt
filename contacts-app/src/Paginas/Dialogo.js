import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';

class Dialogo extends React.Component {
  constructor(props) {
    super(props);
  }

  handleClose = () => {
    this.props.cerrar();
  }

  render() {
    let {titulo, mensaje, open} = this.props;
    return (
      <Dialog
        open = {open}
        onClose={this.handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{titulo}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">{mensaje}</DialogContentText>
        </DialogContent>
        
        <DialogActions>
          <Button onClick={this.handleClose} color="primary">OK</Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default Dialogo;