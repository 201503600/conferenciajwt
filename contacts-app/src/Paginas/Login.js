import React from "react";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import Dialogo from "./Dialogo";

import "../Styles/Login.css";
import ruta from "./Ruta";
import jwt_decode from "jwt-decode";

class Login extends React.Component {
  titulo = "";
  mensaje = "";
  state = {
    email: "",
    password: "",
    openError: false,
  };

  componentDidMount() {
    const token = localStorage.getItem("token");
    if (token !== null) this.verifyRol(token);
  }

  verifyRol = (token) => {
    const userRole = jwt_decode(token);

    const expiracion = userRole.exp;
    if (expiracion < Date.now() / 1000) {
      // El token ha expirado se cierra la sesión
      localStorage.clear();
      return;
    }

    if (userRole.role === "admin") window.location.href = `/admin`;
    else window.location.href = `/inicio`;
  };

  goInicio = async () => {
    let config = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "POST, GET",
        "Access-Control-Request-Method": "*",
        "Access-Control-Allow-Headers":
          "Origin, X-Requested-With, Content-Type, Accept, Authorization",
      },
      body: JSON.stringify({
        email: this.state.email,
        password: this.state.password,
      }),
      mode: "cors",
    };
    //console.log(config.body.photo);
    try {
      let response = await fetch(
        "http://" + ruta.ip + ":" + ruta.port + "/api/login",
        config
      );
      let token = await response.json();
      if (response.status == 200) {
        localStorage.setItem("token", token.token);
        this.verifyRol(token.token);
      } else if (response.status == 400) {
        this.abrirError("Credenciales incorrectas!");
      } else {
        this.abrirError("Ocurrio un error! Intente de nuevo");
      }
    } catch (error) {
      this.abrirError("Ocurrio un error! Intente de nuevo");
    }
  };

  abrirError = (mensaje) => {
    this.titulo = "ERROR";
    this.mensaje = mensaje;
    this.setState({
      openError: true,
    });
  };

  cerrarError = () => {
    this.setState({
      openError: false,
    });
  };

  setEmail = async (e) => {
    this.state.email = e.target.value;
  };

  setPass = async (e) => {
    this.state.password = e.target.value;
  };

  goRegistrar() {
    window.location.href = `/registro`;
  }

  render() {
    return (
      <>
        <Grid container className="root" spacing={2} justifyContent="center">
          <Paper className="paper" elevation={5} justify="center">
            <div style={{ width: "100%", display: "flex" }}>
              <div style={{ width: "30%", paddingTop: "2em" }}>
                <Avatar className="img" src="" alt="Travis Howard" />
              </div>

              <div style={{ width: "80%", paddingLeft: "2em" }}>
                <br />
                <TextField
                  label="Correo"
                  fullWidth
                  margin="normal"
                  onChange={this.setEmail}
                />
                <TextField
                  label="Password"
                  type="password"
                  fullWidth
                  margin="normal"
                  onChange={this.setPass}
                />
              </div>
            </div>

            <Grid
              container
              justifyContent="center"
              style={{ marginLeft: "6em" }}
            >
              <Button
                variant="outlined"
                color="primary"
                onClick={this.goInicio}
              >
                log in
              </Button>
            </Grid>
            <br />
            <br />
            <br />
            <Grid container spacing={2} justifyContent="center">
              <Typography>
                <Link href="#" onClick={this.goRegistrar}>
                  No tienes cuenta? Registrate!
                </Link>
              </Typography>
            </Grid>
          </Paper>
          <Dialogo
            titulo={this.titulo}
            mensaje={this.mensaje}
            open={this.state.openError}
            cerrar={this.cerrarError}
          />
        </Grid>
      </>
    );
  }
}

export default Login;
