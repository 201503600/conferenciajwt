import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
  container: {
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(8),
  },
  grid: {
    height: '100%',
  },
  image: {
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundRepeat: 'no-repeat',
    backgroundColor: theme.palette.grey[50],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  message: {
    margin: theme.spacing(4),
  },
}));

function AccessDenied() {
  const classes = useStyles();

  return (
    <Container className={classes.container} maxWidth="lg">
      <Grid container spacing={4} className={classes.grid}>
        <Grid item xs={12} md={6} className={classes.image}></Grid>
        <Grid item xs={12} md={6} className={classes.message}>
          <Typography variant="h4" component="h1" gutterBottom>
            Acceso denegado
          </Typography>
          <Typography variant="body1" gutterBottom>
            No tienes permisos para acceder a esta página.
          </Typography>
        </Grid>
      </Grid>
    </Container>
  );
}

export default AccessDenied;