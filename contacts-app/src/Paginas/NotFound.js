import React from "react";

import "../Styles/NotFound.css";

class NotFound extends React.Component {
  goRegistrar() {
    window.location.href = `/Registro`;
  }

  render() {
    return (
      <>
        <div className="not-found-page">
          <h1 className="not-found-title">Oops!</h1>
          <p className="not-found-text">
            No se ha encontrado la página.
          </p>
          <img
            src="https://i.imgur.com/zoJLZGk.png"
            alt="Sad face"
            className="not-found-image"
          />
        </div>
      </>
    );
  }
}

export default NotFound;
