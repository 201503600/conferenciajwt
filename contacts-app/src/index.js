import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Login from './Paginas/Login';
import Registro from './Paginas/Registro';
import Inicio from './Paginas/Inicio';
import Admin from './Paginas/Admin';
import NotFound from './Paginas/NotFound';
import { Redirect } from 'react-router-dom/cjs/react-router-dom.min';
import AccessDenied from './Paginas/AccessDenied';
import Contacto from './Paginas/Contacto';

ReactDOM.render(
  
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={() => <Redirect to="/login"/>} />
        <Route exact path = '/login' component={Login} />
        <Route exact path = '/registro' component={Registro} />
        <Route exact path = '/admin' component={Admin} />
        <Route exact path = '/inicio' component={Inicio} />
        <Route exact path = '/contact' component={Contacto} />
        <Route exact path = '/acces-denied' component={AccessDenied} />
        <Route path="*" component={NotFound} />
      </Switch>
    </BrowserRouter>
  ,
  document.getElementById('root')
);
