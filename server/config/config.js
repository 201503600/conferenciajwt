import { config } from "dotenv";
config();

export const PORT = process.env.PORT || 4000;
export const JWT_SECRET = process.env.SECRET_KEY || "conferencia-jwt";
export const DB_HOST = process.env.DB_HOST || "conferencia-jwt.cprbiftrqe9m.us-east-2.rds.amazonaws.com";
export const DB_USER = process.env.DB_USER || "admin";
export const DB_PASSWORD = process.env.DB_PASSWORD || "admin12345";
export const DB_DATABASE = process.env.DB_DATABASE || "conferenciaJWT";
export const DB_PORT = process.env.DB_PORT || 3306;
export const BCRYPT_SALT_ROUNDS = 12;