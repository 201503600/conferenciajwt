import { PORT } from "./config/config.js";
import { getUsers, updateUser } from "./controller/admin-controller.js";
import { createContact, deleteContact, getContacts } from "./controller/user-controller.js";
import { login, register, validate } from "./middleware.js";
import express from 'express';
import bodyParser from "body-parser";
import cors from 'cors';

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.post("/api/login", login);
app.post("/api/register", register);
app.get("/api/admin", validate, getUsers);
app.put("/api/admin/:id", validate, updateUser);
app.get("/api/users", validate, getContacts);
app.post("/api/users", validate, createContact);
app.delete("/api/users/:idContact", validate, deleteContact);

app.use((req, res, next) => {
  res.status(404).json({ message: "Not found" });
});
app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send("Something went wrong!");
});

app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
