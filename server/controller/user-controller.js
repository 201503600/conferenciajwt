import { executeQuery } from "../config/db.js";

export const getContacts = async (req, res) => {
  const { id } = req.user;

  executeQuery(`SELECT id, name, phone FROM contacts WHERE user_id = ${id}`)
    .then((results) => {
      res.status(200).json(results);
    })
    .catch((error) => {
      throw error;
    });
};

export const createContact = async (req, res) => {
  const { id } = req.user;
  const { name, phone } = req.body;

  executeQuery(
    `INSERT INTO contacts (name, phone, user_id) VALUES ('${name}', ${phone}, ${id})`
  )
    .then((_) => {
      res.status(200).json({ message: "Contact added" });
    })
    .catch((error) => {
      throw error;
    });
};

export const deleteContact = async (req, res) => {
  const { id } = req.user;
  const { idContact } = req.params;

  executeQuery(
    `DELETE FROM contacts WHERE id = ${idContact} AND user_id = ${id}`
  )
    .then((_) => {
      res.status(200).json({ message: "Contact deleted" });
    })
    .catch((error) => {
      throw error;
    });
};
