import { executeQuery } from "../config/db.js";

export const getUsers = async (req, res) => {
  const { role } = req.user;
  
  if (role !== "admin") {
    return res.status(403).json({ message: "Forbidden" });
  }

  executeQuery(`SELECT id, email, status FROM users WHERE rol != 'admin'`)
    .then((results) => {
      res.status(200).json(results);
    })
    .catch((error) => {
      throw error;
    });
};

export const updateUser = async (req, res) => {
  const { role } = req.user;

  if (role !== "admin") {
    return res.status(403).json({ message: "Forbidden" });
  }

  const { id } = req.params;
  const { status } = req.body;

  executeQuery(`UPDATE users SET status = '${status}' WHERE id = ${id}`)
    .then(_ => {
      res.status(200).json({ message: "User updated" });
    })
    .catch((error) => {
      throw error;
    });
};
