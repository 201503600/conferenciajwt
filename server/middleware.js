import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { BCRYPT_SALT_ROUNDS, JWT_SECRET } from "./config/config.js";
import { executeQuery } from "./config/db.js";

export const login = async (req, res) => {
  const { email, password } = req.body;

  executeQuery(`SELECT * FROM users WHERE email = '${email}' AND status = 'approved'`)
    .then((results) => {
      if (results.length === 0) {
        return res.status(401).json({ message: "User not found" });
      }
      const user = results[0];

      bcrypt.compare(password, user.password, (err, result) => {
        if (err) throw err;

        if (!result) {
          return res.status(401).json({ message: "Incorrect password" });
        }

        const token = jwt.sign({ id: user.id, role: user.rol }, JWT_SECRET, {
          expiresIn: "5m",
        });

        res.json({ token });
      });
    })
    .catch((error) => {
      throw error;
    });
};

export const register = async (req, res) => {
  const { email, pass } = req.body;
  const password = await bcrypt.hash(pass, BCRYPT_SALT_ROUNDS);

  executeQuery(
    `INSERT INTO users (email, password) VALUES ('${email}','${password}')`
  )
    .then((_) => {
      res.status(200).json({ message: "User registered" });
    })
    .catch((error) => {
      throw error;
    });
};

export const validate = (req, res, next) => {
  const token = req.headers.authorization.split(' ')[1];

  if (!token) {
      return res.status(401).json({ message: "Unauthorized" });
  }

  try {
    const decoded = jwt.verify(token, JWT_SECRET);
    req.user = decoded;
    if (req.path.includes("/api/admin/") && req.user.role !== "admin") {
      return res.status(403).json({ message: "Forbidden" });
    }

    if (req.path.includes("/api/users/") && req.user.role !== "user") {
      return res.status(403).json({ message: "Forbidden" });
    }
    next();
  } catch (err) {
    res.status(401).json({ message: "Invalid token" });
  }
};
