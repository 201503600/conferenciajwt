# ConferenciaJWT

##  Server

API Rest creada en Node JS implementando JWT para la autenticación y autorización.
Ejecutar `npm install` para la instalación de dependencias.
Ejecutar `npm start`.

## Contact-app

Aplicación web creada en React v17.0.1
Ejecutar `npm install` para la instalación de dependencias.
Ejecutar `npm start`.

## Base de datos

El script de la base de datos se encuentra en el documento `database.sql`.
